package com.ub.ubgeolocation.geoLite.services;

import com.ub.core.base.utils.StringUtils;
import com.ub.ubgeolocation.geoLite.models.GeoLiteBlockIPDoc;
import com.ub.ubgeolocation.geoLite.models.GeoLiteLocationDoc;
import com.ub.ubgeolocation.geolocation.models.GeolocationDoc;
import com.ub.ubgeolocation.geolocation.models.UpdateStatusEnum;
import com.ub.ubgeolocation.geolocation.services.GeolocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Date;

@Component
public class GeoLiteService {

    @Autowired private CIDRServices cidrServices;
    @Autowired private GeoLiteBlockIPService geoLiteBlockIPService;
    @Autowired private GeoLiteLocationService geoLiteLocationService;
    @Autowired private GeolocationService geolocationService;

    public GeoLiteLocationDoc getLocationByIP(String ip) {
        GeoLiteBlockIPDoc geoLiteBlockIPDoc = geoLiteBlockIPService.findByIp(ip);
        if (geoLiteBlockIPDoc == null) {
            return null;
        }

        return geoLiteLocationService.findByGeoId(geoLiteBlockIPDoc.getGeoId());
    }

    public boolean startUpdate(final String pathIPv4Blocks, final String pathIPv6Blocks,
                               final String pathLocations, final String countryIsoCode) {
        if (geolocationService.findInProgress().size() > 0) {
            return false;
        }

        Thread thread = new Thread() {
            @Override
            public void run() {
                GeolocationDoc geolocationDoc = new GeolocationDoc();
                geolocationDoc.setUpdateStatus(UpdateStatusEnum.IN_PROGRESS);
                geolocationDoc.setStart(new Date());
                geolocationDoc = geolocationService.save(geolocationDoc);

                try {
                    updateGeoLiteLocations(pathLocations, countryIsoCode);
                    updateGeoLiteIPBlocks(pathIPv4Blocks, pathIPv6Blocks);
                    geolocationDoc.setUpdateStatus(UpdateStatusEnum.COMPLETE);
                } catch (Exception e) {
                    geolocationDoc.setUpdateStatus(UpdateStatusEnum.ERROR);
                    geolocationDoc.setErrorMsg(e.toString());
                }

                geolocationDoc.setEnd(new Date());
                geolocationService.save(geolocationDoc);
            }
        };

        thread.start();

        return true;
    }

    private void updateGeoLiteIPBlocks(String pathIPv4Blocks, String pathIPv6Blocks) throws IOException {
        geoLiteBlockIPService.removeAll();
        addGeoLiteIPBlocks(pathIPv4Blocks);
        addGeoLiteIPBlocks(pathIPv6Blocks);
    }

    private void addGeoLiteIPBlocks(String path) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(new File(path)), "UTF-8"
        ));

        boolean isFirst = true;
        for (String line; (line = br.readLine()) != null; ) {
            // skip header
            if (isFirst) {
                isFirst = false;
                continue;
            }

            String[] params = line.replace("\"", "").split(",");
            if (params.length >= 2) {
                try {
                    Integer geoId = Integer.parseInt(params[1]);
                    GeoLiteLocationDoc geoLiteLocationDoc = geoLiteLocationService.findByGeoId(geoId);

                    if (geoLiteLocationDoc != null) {
                        /*  network, geoname_id, registered_country_geoname_id, represented_country_geoname_id,
                        is_anonymous_proxy, is_satellite_provider, postal_code, latitude, longitude, accuracy_radius */
                        GeoLiteBlockIPDoc geoLiteBlockIPDoc = new GeoLiteBlockIPDoc();
                        geoLiteBlockIPDoc.setAddressRange(cidrServices.parseCIDR(params[0]));
                        geoLiteBlockIPDoc.setGeoId(geoId);

                        geoLiteBlockIPService.save(geoLiteBlockIPDoc);
                    }
                } catch (NumberFormatException ignored) {
                }
            }
        }
    }

    private void updateGeoLiteLocations(String path, String countryIsoCode) throws IOException {
        geoLiteLocationService.removeAll();
        addGeoLiteLocations(path, countryIsoCode);
    }

    private void addGeoLiteLocations(String path, String countryIsoCode) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(new File(path)), "UTF-8"
        ));

        boolean isFirst = true;
        for (String line; (line = br.readLine()) != null; ) {
            // skip header
            if (isFirst) {
                isFirst = false;
                continue;
            }

            String[] params = line.replace("\"", "").split(",");
            if (params.length >= 11) {
                try {
                    Integer geoId = Integer.parseInt(params[0]);
                    if (StringUtils.isEmpty(countryIsoCode) || params[4].equals(countryIsoCode)) {
                        /*  geoname_id, locale_code, continent_code, continent_name,country_iso_code, country_name,
                        subdivision_1_iso_code, subdivision_1_name,subdivision_2_iso_code, subdivision_2_name,
                        city_name, metro_code, time_zone */
                        GeoLiteLocationDoc geoLiteLocationDoc = new GeoLiteLocationDoc();
                        geoLiteLocationDoc.setGeoId(Integer.parseInt(params[0]));
                        geoLiteLocationDoc.setCountryName(params[4]);
                        geoLiteLocationDoc.setRegionName(params[7]);
                        geoLiteLocationDoc.setCityName(params[10]);

                        geoLiteLocationService.save(geoLiteLocationDoc);
                    }
                } catch (NumberFormatException ignored) {
                }
            }
        }
    }
}
