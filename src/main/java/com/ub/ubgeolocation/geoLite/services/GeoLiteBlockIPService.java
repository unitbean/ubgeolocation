package com.ub.ubgeolocation.geoLite.services;

import com.ub.ubgeolocation.geoLite.models.GeoLiteBlockIPDoc;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class GeoLiteBlockIPService {

    @Autowired private MongoTemplate mongoTemplate;


    public GeoLiteBlockIPDoc save(GeoLiteBlockIPDoc geoLiteBlockIPDoc) {
        mongoTemplate.save(geoLiteBlockIPDoc);
        return geoLiteBlockIPDoc;
    }

    public GeoLiteBlockIPDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, GeoLiteBlockIPDoc.class);
    }

    public GeoLiteBlockIPDoc findByGeonameId(String id) {
        Criteria criteria = new Criteria("geonameId").is(id);
        return mongoTemplate.findOne(new Query(criteria), GeoLiteBlockIPDoc.class);
    }

    public GeoLiteBlockIPDoc findByIp(String ip) {

        try {
            InetAddress address = InetAddress.getByName(ip);
            BigInteger target = new BigInteger(1, address.getAddress());

            return mongoTemplate.findOne(
                    new Query(Criteria.where("addressRange.start")
                            .lte(target).and("addressRange.end").gte(target)),
                    GeoLiteBlockIPDoc.class
            );
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void remove(ObjectId id) {
        GeoLiteBlockIPDoc geoLiteBlockIPDoc = findById(id);
        if (geoLiteBlockIPDoc == null) return;
        mongoTemplate.remove(geoLiteBlockIPDoc);
    }

    public void removeAll() {
        mongoTemplate.dropCollection(GeoLiteBlockIPService.class);
    }
}
