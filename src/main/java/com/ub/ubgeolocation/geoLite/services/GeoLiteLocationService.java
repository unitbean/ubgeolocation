package com.ub.ubgeolocation.geoLite.services;

import com.ub.ubgeolocation.geoLite.models.GeoLiteLocationDoc;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class GeoLiteLocationService {

    @Autowired private MongoTemplate mongoTemplate;

    public GeoLiteLocationDoc save(GeoLiteLocationDoc geoLiteLocationDoc){
            mongoTemplate.save(geoLiteLocationDoc);
            return geoLiteLocationDoc;
    }

    public GeoLiteLocationDoc findById(ObjectId id){
            return mongoTemplate.findById(id, GeoLiteLocationDoc.class);
    }


    public GeoLiteLocationDoc findByGeoId(Integer geoId){
        Criteria criteria = Criteria.where("geoId").is(geoId);
        return mongoTemplate.findOne(new Query(criteria), GeoLiteLocationDoc.class);
    }

    public void remove(ObjectId id){
        GeoLiteLocationDoc geoLiteLocationDoc = findById(id);
        if (geoLiteLocationDoc == null) return;
        mongoTemplate.remove(geoLiteLocationDoc);
    }

    public void removeAll() {
        mongoTemplate.dropCollection(GeoLiteLocationDoc.class);
    }
}
