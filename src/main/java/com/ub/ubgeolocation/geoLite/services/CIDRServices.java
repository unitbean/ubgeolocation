package com.ub.ubgeolocation.geoLite.services;

import com.ub.ubgeolocation.geoLite.models.AddressRangeModel;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

@Component
public class CIDRServices {

    public AddressRangeModel parseCIDR(String cidr) throws UnknownHostException {

        if (cidr.contains("/")) {
            int index = cidr.indexOf("/");
            String addressPart = cidr.substring(0, index);
            String networkPart = cidr.substring(index + 1);

            InetAddress inetAddress = InetAddress.getByName(addressPart);
            int prefix = Integer.parseInt(networkPart);

            return calculate(inetAddress, prefix);
        } else {
            throw new IllegalArgumentException("Not a CIDR format");
        }
    }


    private AddressRangeModel calculate(InetAddress inetAddress, int prefix) throws UnknownHostException {

        ByteBuffer maskBuffer;
        if (inetAddress.getAddress().length == 4) {
            maskBuffer = ByteBuffer.allocate(4).putInt(-1);
        } else {
            maskBuffer = ByteBuffer.allocate(16).putLong(-1L).putLong(-1L);
        }

        BigInteger mask = (new BigInteger(1, maskBuffer.array())).not().shiftRight(prefix);

        ByteBuffer buffer = ByteBuffer.wrap(inetAddress.getAddress());
        BigInteger ipValue = new BigInteger(1, buffer.array());

        BigInteger startIp = ipValue.and(mask);
        BigInteger endIp = startIp.add(mask.not());

        AddressRangeModel addressRange = new AddressRangeModel();
        addressRange.setStart(startIp);
        addressRange.setEnd(endIp);
        return addressRange;
    }
}
