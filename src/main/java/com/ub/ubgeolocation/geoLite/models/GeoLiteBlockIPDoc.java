package com.ub.ubgeolocation.geoLite.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class GeoLiteBlockIPDoc {
    @Id
    private ObjectId id;
    private AddressRangeModel addressRange;
    private Integer geoId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public AddressRangeModel getAddressRange() {
        return addressRange;
    }

    public void setAddressRange(AddressRangeModel addressRange) {
        this.addressRange = addressRange;
    }

    public Integer getGeoId() {
        return geoId;
    }

    public void setGeoId(Integer geoId) {
        this.geoId = geoId;
    }
}
