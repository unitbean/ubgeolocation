package com.ub.ubgeolocation.geoLite.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.base.views.pageHeader.PageHeader;
import com.ub.ubgeolocation.geoLite.routes.GeoLiteAdminRoutes;
import com.ub.ubgeolocation.geoLite.services.GeoLiteService;
import com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class GeoLiteAdminController {

    @Autowired private GeoLiteService geoLiteService;

    private PageHeader defaultPageHeader(String current) {
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

    @RequestMapping(value = GeoLiteAdminRoutes.UPDATE, method = RequestMethod.GET)
    public String updateDB(Model model) {
        model.addAttribute("pageHeader", defaultPageHeader("Обновление базы"));
        return "com.ub.ubgeolocation.admin.geoLite.update";
    }

    @RequestMapping(value = GeoLiteAdminRoutes.UPDATE, method = RequestMethod.POST)
    public String updateDB(@RequestParam String pathIPv4Blocks, @RequestParam String pathIPv6Blocks,
                           @RequestParam String pathLocations, @RequestParam String countryIsoCode,
                           RedirectAttributes redirectAttributes) {
        boolean success = geoLiteService.startUpdate(pathIPv4Blocks, pathIPv6Blocks, pathLocations, countryIsoCode);
        redirectAttributes.addAttribute("success", success);
        return RouteUtils.redirectTo(GeolocationAdminRoutes.ALL);
    }
}
