package com.ub.ubgeolocation.geoLite.routes;

import com.ub.core.base.routes.BaseRoutes;

public class GeoLiteAdminRoutes {
    private static final String ROOT = BaseRoutes.ADMIN + "/geoLite";

    public static final String UPDATE = ROOT + "/update";
    public static final String UPDATE_IPV4 = UPDATE + "/ipv4";
    public static final String UPDATE_IPV6 = UPDATE + "/ipv6";
    public static final String UPDATE_LOCATIONS = UPDATE + "/locations";

    private static final String LOCATIONS = ROOT + "/locations";
    public static final String ADD = LOCATIONS + "/add";
    public static final String EDIT = LOCATIONS + "/edit";
    public static final String ALL = LOCATIONS + "/all";
    public static final String DELETE = LOCATIONS + "/delete";
    public static final String MODAL_PARENT = LOCATIONS + "/modal/response";
}
