package com.ub.ubgeolocation.geolocation.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.base.views.breadcrumbs.BreadcrumbsLink;
import com.ub.core.base.views.pageHeader.PageHeader;
import com.ub.ubgeolocation.geoLite.models.GeoLiteLocationDoc;
import com.ub.ubgeolocation.geoLite.routes.GeoLiteAdminRoutes;
import com.ub.ubgeolocation.geolocation.models.GeolocationDoc;
import com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes;
import com.ub.ubgeolocation.geolocation.services.GeolocationService;
import com.ub.ubgeolocation.geolocation.views.all.SearchGeolocationAdminRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class GeolocationAdminController {
    @Autowired private GeolocationService geolocationService;

    private PageHeader defaultPageHeader(String current){
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.setLinkAdd(GeoLiteAdminRoutes.UPDATE);
        pageHeader.setTitleAdd("Обновить базу");
        pageHeader.getBreadcrumbs().getLinks().add(new BreadcrumbsLink("#","Геолокация"));
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

    @RequestMapping(value = GeolocationAdminRoutes.SHOW, method = RequestMethod.GET)
    public String show(@RequestParam ObjectId id, Model model) {
        GeolocationDoc geolocationDoc = geolocationService.findById(id);
        model.addAttribute("geolocationDoc", geolocationDoc);
        model.addAttribute("pageHeader", defaultPageHeader("Подробнее"));
        return "com.ub.ubgeolocation.admin.geolocation.show";
    }

    @RequestMapping(value = GeolocationAdminRoutes.ALL, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                      @RequestParam(required = false, defaultValue = "") String query,
                      Model model) {
        SearchGeolocationAdminRequest searchGeolocationAdminRequest = new SearchGeolocationAdminRequest(currentPage);
        searchGeolocationAdminRequest.setQuery(query);
        model.addAttribute("searchGeolocationAdminResponse", geolocationService.findAll(searchGeolocationAdminRequest));
        model.addAttribute("pageHeader",defaultPageHeader("Все обновления"));
        return "com.ub.ubgeolocation.admin.geolocation.all";
    }

    @RequestMapping(value = GeolocationAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("pageHeader",defaultPageHeader("Удаление"));
        return "com.ub.ubgeolocation.admin.geolocation.delete";
    }

    @RequestMapping(value = GeolocationAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        geolocationService.remove(id);
        return RouteUtils.redirectTo(GeolocationAdminRoutes.ALL);
    }

    @RequestMapping(value = GeolocationAdminRoutes.TEST, method = RequestMethod.GET)
    public String test(@RequestParam(required = false) String reqIp,
                       Model model) {

        String ip = geolocationService.findIp();
        GeoLiteLocationDoc geoLiteLocationDoc = geolocationService.findLocationByIp(ip);

        model.addAttribute("ip", ip);
        if(geoLiteLocationDoc != null) {
            model.addAttribute("city", geoLiteLocationDoc.getCityName());
            model.addAttribute("region", geoLiteLocationDoc.getRegionName());
        }

        if(reqIp != null) {
            geoLiteLocationDoc = geolocationService.findLocationByIp(reqIp);
            if (geoLiteLocationDoc != null) {
                model.addAttribute("succ", true);
                model.addAttribute("reqCity", geoLiteLocationDoc.getCityName());
                model.addAttribute("reqRegion", geoLiteLocationDoc.getRegionName());
            } else {
                model.addAttribute("succ", false);
            }
        }
        model.addAttribute("pageHeader",defaultPageHeader("Тестирование"));
        return "com.ub.ubgeolocation.admin.geolocation.test";
    }

    @RequestMapping(value = GeolocationAdminRoutes.TEST, method = RequestMethod.POST)
    public String test(@RequestParam String reqIp,
                       RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("reqIp", reqIp);

        return RouteUtils.redirectTo(GeolocationAdminRoutes.TEST);
    }
}
