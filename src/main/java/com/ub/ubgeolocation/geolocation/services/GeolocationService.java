package com.ub.ubgeolocation.geolocation.services;

import com.ub.ubgeolocation.geoLite.models.GeoLiteLocationDoc;
import com.ub.ubgeolocation.geoLite.services.GeoLiteService;
import com.ub.ubgeolocation.geolocation.events.IGeolocationEvent;
import com.ub.ubgeolocation.geolocation.models.GeolocationDoc;
import com.ub.ubgeolocation.geolocation.models.UpdateStatusEnum;
import com.ub.ubgeolocation.geolocation.views.all.SearchGeolocationAdminRequest;
import com.ub.ubgeolocation.geolocation.views.all.SearchGeolocationAdminResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class GeolocationService {
    private static Map<String, IGeolocationEvent> geolocationEvents = new HashMap<String, IGeolocationEvent>();

    @Autowired private MongoTemplate mongoTemplate;
    @Autowired private GeoLiteService geoLiteService;

    public static void addGeolocationEvent(IGeolocationEvent iGeolocationEvent) {
        geolocationEvents.put(iGeolocationEvent.getClass().getCanonicalName(), iGeolocationEvent);
    }

    public GeolocationDoc save(GeolocationDoc geolocationDoc) {
        mongoTemplate.save(geolocationDoc);
        callAfterSave(geolocationDoc);
        return geolocationDoc;
    }

    public GeolocationDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, GeolocationDoc.class);
    }

    public List<GeolocationDoc> findInProgress() {
        return mongoTemplate.find(new Query(Criteria.where("updateStatus").is(UpdateStatusEnum.IN_PROGRESS)), GeolocationDoc.class);
    }

    public void remove(ObjectId id) {
        GeolocationDoc geolocationDoc = findById(id);
        if (geolocationDoc == null) return;
        mongoTemplate.remove(geolocationDoc);
        callAfterDelete(geolocationDoc);
    }

    public SearchGeolocationAdminResponse findAll(SearchGeolocationAdminRequest searchGeolocationAdminRequest) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(
                searchGeolocationAdminRequest.getCurrentPage(),
                searchGeolocationAdminRequest.getPageSize(),
                sort);

        Criteria criteria = new Criteria();

        Criteria.where("id").regex(searchGeolocationAdminRequest.getQuery(), "i");

        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, GeolocationDoc.class);
        query = query.with(pageable);

        List<GeolocationDoc> result = mongoTemplate.find(query, GeolocationDoc.class);
        SearchGeolocationAdminResponse searchGeolocationAdminResponse = new SearchGeolocationAdminResponse(
                searchGeolocationAdminRequest.getCurrentPage(),
                searchGeolocationAdminRequest.getPageSize(),
                result);
        searchGeolocationAdminResponse.setAll(count);
        searchGeolocationAdminResponse.setQuery(searchGeolocationAdminRequest.getQuery());
        return searchGeolocationAdminResponse;
    }

    private void callAfterSave(GeolocationDoc geolocationDoc) {
        for (IGeolocationEvent iGeolocationEvent : geolocationEvents.values()) {
            iGeolocationEvent.afterSave(geolocationDoc);
        }
    }

    private void callAfterDelete(GeolocationDoc geolocationDoc) {
        for (IGeolocationEvent iGeolocationEvent : geolocationEvents.values()) {
            iGeolocationEvent.afterDelete(geolocationDoc);
        }
    }

    public String findIp() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        String ip = request.getHeader("X-Real-IP");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

    public GeoLiteLocationDoc findLocationByIp(String ip) {
        return geoLiteService.getLocationByIP(ip);
    }

    public GeoLiteLocationDoc findLocation() {
        return findLocationByIp(findIp());
    }
}
