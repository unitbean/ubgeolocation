package com.ub.ubgeolocation.geolocation.routes;

import com.ub.core.base.routes.BaseRoutes;

public class GeolocationAdminRoutes {
    private static final String ROOT = BaseRoutes.ADMIN + "/geolocation";

    public static final String SHOW = ROOT + "/show";
    public static final String ALL = ROOT + "/all";
    public static final String DELETE = ROOT + "/delete";
    public static final String TEST = ROOT + "/test";
}
