package com.ub.ubgeolocation.geolocation.events;

import com.ub.ubgeolocation.geolocation.models.GeolocationDoc;

public interface IGeolocationEvent {
    public void preSave(GeolocationDoc geolocationDoc);
    public void afterSave(GeolocationDoc geolocationDoc);

    public Boolean preDelete(GeolocationDoc geolocationDoc);
    public void afterDelete(GeolocationDoc geolocationDoc);
}
