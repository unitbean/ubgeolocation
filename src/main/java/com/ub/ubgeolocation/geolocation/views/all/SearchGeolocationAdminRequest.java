package com.ub.ubgeolocation.geolocation.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchGeolocationAdminRequest extends SearchRequest{
    public SearchGeolocationAdminRequest() {
    }

    public SearchGeolocationAdminRequest(Integer currentPage){
        this.currentPage = currentPage;
    }

    public SearchGeolocationAdminRequest(Integer currentPage, Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
