package com.ub.ubgeolocation.geolocation.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.ubgeolocation.geolocation.models.GeolocationDoc;

import java.util.List;

public class SearchGeolocationAdminResponse extends SearchResponse {
    private List<GeolocationDoc> result;


    public SearchGeolocationAdminResponse() {
    }

    public SearchGeolocationAdminResponse(Integer currentPage, Integer pageSize, List<GeolocationDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<GeolocationDoc> getResult() {
        return result;
    }

    public void setResult(List<GeolocationDoc> result) {
        this.result = result;
    }
}
