package com.ub.ubgeolocation.geolocation.models;

import com.ub.core.base.models.BaseModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;

@Document
public class GeolocationDoc extends BaseModel {
    @Id
    private ObjectId id;
    private Date start;
    private Date end;
    private UpdateStatusEnum updateStatus;
    private String errorMsg;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public UpdateStatusEnum getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(UpdateStatusEnum updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


}
