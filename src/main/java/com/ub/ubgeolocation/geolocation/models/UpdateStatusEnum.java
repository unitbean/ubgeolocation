package com.ub.ubgeolocation.geolocation.models;


public enum UpdateStatusEnum {
    IN_PROGRESS("В процессе"), ERROR("Ошибка"), COMPLETE("Завершен");

    UpdateStatusEnum(String title) {
        this.title = title;
    }

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
