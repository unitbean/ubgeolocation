package com.ub.ubgeolocation.geolocation.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes;

public class GeolocationTestMenu extends CoreMenu {
    public GeolocationTestMenu() {
        this.name ="Тестирование";
        this.parent = new GeolocationMenu();
        this.url = GeolocationAdminRoutes.TEST;
    }
}
