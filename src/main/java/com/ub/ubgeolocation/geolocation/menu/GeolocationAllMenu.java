package com.ub.ubgeolocation.geolocation.menu;

import com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class GeolocationAllMenu extends CoreMenu {
    public GeolocationAllMenu() {
        this.name ="Обновления базы";
        this.parent = new GeolocationMenu();
        this.url = GeolocationAdminRoutes.ALL;
    }
}
