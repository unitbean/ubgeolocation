package com.ub.ubgeolocation.geolocation.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class GeolocationMenu extends CoreMenu{
    public GeolocationMenu() {
        this.name = "Геолокация";
        this.icon = MenuIcons.ENTYPO_LOGO_DB;
    }
}
