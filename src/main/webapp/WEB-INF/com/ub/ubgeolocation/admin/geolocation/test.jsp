<%@ page import="com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/com/ub/core/admin/main/components/pageHeader.jsp"/>

<div class="widget widget-blue">
    <div class="widget-title">
        <h3><i class="fa fa-table" aria-hidden="true"></i> Тестирование: </h3>
    </div>
    <div class="widget-content">
        <div class="row">
            <div class="alert alert-info" role="alert">Ваш IP адресс: ${ip}</div>
            <c:if test="${empty region}">
                <div class="alert alert-warning" role="alert">Мы ничего не нашли по Вашему IP адресу.</div>
            </c:if>
            <c:if test="${not empty region}">
                <div class="alert alert-info" role="alert">Ваш регион: ${region}, город: ${city}</div>
            </c:if>
            <c:if test="${not empty reqRegion}">
                <c:if test="${succ}">
                    <div class="alert alert-success" role="alert">
                        Ваш запрос обработан, регион: ${reqRegion}, город: ${reqCity}
                    </div>
                </c:if>
                <c:if test="${not succ}">
                    <div class="alert alert-danger" role="alert">По Вашему запросу ничего не найдено!</div>
                </c:if>
            </c:if>

            <form:form action="<%=GeolocationAdminRoutes.TEST%>" method="POST">

                <div class="row">
                    <div class="col-lg-12">
                        <label for="reqIp">IP Адрес для проверки:</label>
                        <input name="reqIp" class="form-control" id="reqIp"/>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <button type="submit" id="contact-form-settings-submit" class="btn btn-primary">Отправить</button>
                </div>
            </form:form>
        </div>
    </div>
</div>

