<%@ page import="com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/com/ub/core/admin/main/components/pageHeader.jsp"/>

<div class="widget widget-blue">
    <div class="widget-title">
        <h3><i class="fa fa-table" aria-hidden="true"></i> Список обновлений: </h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-bordered" id="table-1">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Начало</th>
                    <th>Конец</th>
                    <th>Статус обновления</th>

                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${searchGeolocationAdminResponse.result}" var="doc">

                    <tr>
                        <td>${doc.id}</td>
                        <td>${doc.start}</td>
                        <td>${doc.end}</td>
                        <td>${doc.updateStatus.title}</td>

                        <td class="">
                            <c:url value="<%= GeolocationAdminRoutes.SHOW%>" var="showUrl">
                                <c:param name="id" value="${doc.id}"/>
                            </c:url>
                            <a href="${showUrl}" class="btn btn-default btn-xs">
                                <i class="icon-pencil">Подробнее</i>
                            </a>
                            <c:url value="<%= GeolocationAdminRoutes.DELETE%>" var="urlDelete">
                                <c:param name="id" value="${doc.id}"></c:param>
                            </c:url>
                            <a href="${urlDelete}" type="submit" class="btn btn-xs btn-danger  ">
                                <i class="fa fa-times-circle" aria-hidden="true"></i> Удалить
                            </a>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">
            <c:url value="<%= GeolocationAdminRoutes.ALL%>" var="urlPrev">
                <c:param name="query" value="${searchGeolocationAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchGeolocationAdminResponse.prevNum()}"/>
            </c:url>
            <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i></a></li>

            <c:forEach items="${searchGeolocationAdminResponse.paginator()}" var="page">
                <c:url value="<%= GeolocationAdminRoutes.ALL%>" var="urlPage">
                    <c:param name="query" value="${searchGeolocationAdminResponse.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="<c:if test="${searchGeolocationAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="${urlPage}">${page + 1}</a>
                </li>
            </c:forEach>
            <c:url value="<%= GeolocationAdminRoutes.ALL%>" var="urlNext">
                <c:param name="query" value="${searchGeolocationAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchGeolocationAdminResponse.nextNum()}"/>
            </c:url>
            <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i></a></li>
        </ul>
    </div>
</div>
