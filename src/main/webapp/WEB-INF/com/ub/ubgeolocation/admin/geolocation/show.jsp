<%@ page import="com.ub.ubgeolocation.geolocation.routes.GeolocationAdminRoutes" %>
<%@ page import="com.ub.ubgeolocation.geolocation.models.UpdateStatusEnum" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/com/ub/core/admin/main/components/pageHeader.jsp"/>
<c:set var="ERROR" value="<%=UpdateStatusEnum.ERROR%>"/>

<div class="row">
    <div class="col-lg-12">
        <div class="widget widget-green" id="widget_profit_chart">
            <div class="widget-title">
                <h3><i class="fa fa-tasks" aria-hidden="true"></i> Редактировать </h3>
            </div>
            <div class="widget-content">
                <c:if test="${not empty success}">
                    <c:if test="${success}">
                        <div class="alert alert-success" role="alert">Обновление началось!</div>
                    </c:if>
                    <c:if test="${not success}">
                        <div class="alert alert-error" role="alert">Обновление уже запущено!</div>
                    </c:if>
                </c:if>

                <div class="row">
                    <div class="col-lg-12">
                        <label for="start">Начало</label>
                        <input value="${geolocationDoc.start}" type="text" class="form-control" id="start" readonly/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <label for="end">Конец</label>
                        <input value="${geolocationDoc.end}" type="text" class="form-control" id="end" readonly/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <label for="updateStatus">Статус обновления</label>
                        <input value="${geolocationDoc.updateStatus}" type="text" class="form-control" id="updateStatus"
                               readonly/>
                    </div>
                </div>

                <c:if test="${geolocationDoc.updateStatus eq ERROR}">
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="errorMsg">Сообщение ошибки</label>
                            <textarea class="form-control" id="errorMsg">${geolocationDoc.errorMsg}</textarea>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>