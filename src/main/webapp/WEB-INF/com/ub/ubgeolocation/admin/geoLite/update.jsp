<%@ page import="com.ub.ubgeolocation.geoLite.routes.GeoLiteAdminRoutes" %>
<%@ page import="com.ub.ubgeolocation.geoLite.routes.GeoLiteRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/com/ub/core/admin/main/components/pageHeader.jsp"/>
<div class="row">
    <div class="col-lg-12">
        <div class="widget widget-green" id="widget_profit_chart">
            <div class="widget-title">
                <h3><i class="fa fa-exclamation" aria-hidden="true"></i> Обновить базу Geo Lite</h3>
            </div>
            <div class="widget-content">
                <form:form action="<%= GeoLiteAdminRoutes.UPDATE%>" method="POST">

                    <div class="alert alert-info" role="alert">Обновление может занять много времени</div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label>Укажите пути на сервере до csv баз GeoLite2</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label for="pathIPv4Blocks">Путь до IPv4Blocks*:</label>
                            <input name="pathIPv4Blocks" class="form-control" id="pathIPv4Blocks"
                                        required="required"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label for="pathIPv6Blocks">Путь до IPv6Blocks*:</label>
                            <input name="pathIPv6Blocks" class="form-control" id="pathIPv6Blocks"
                                        required="required"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label for="pathLocations">Путь до Locations*:</label>
                            <input name="pathLocations" class="form-control" id="pathLocations"
                                        required="required"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="countryIsoCode">Country Iso Code:</label>
                            <input name="countryIsoCode" class="form-control" id="countryIsoCode"/>
                        </div>
                    </div>

                    <br>
                    <div class="form-group">
                        <button type="submit" id="contact-form-settings-submit" class="btn btn-primary">Обновить
                        </button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>